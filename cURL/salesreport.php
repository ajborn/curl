<?php

$THE_PATHS = get_include_path();
set_include_path(
    get_include_path() . PATH_SEPARATOR .
    __DIR__ . DIRECTORY_SEPARATOR . 'Services_Capsule'
);

require_once('Services/Capsule.php');

// include the pagination class
//require_once('Zebra_Pagination/Zebra_Pagination.php');
/**
 * Declares a new Services_Capsule and sends two
 * arguments ('capsule_username', 'capsule_id')
 */
$capsule = new Services_Capsule('xxxxxxxxxxx', 'xxxxxxxxxxxx');

/*
 * Trys to retrieve a List of opps from
 * the capsule server.  The request passes through
 * the API Wrapper.
 */
try {
   $opp_list = $capsule->opportunity->getList();
} catch (Services_Capsule_Exception $e) {
    // print_r($e);
}

//Select the a specific array in the response
$opportunity_list = (!empty($opp_list)) ? $opp_list->opportunities->opportunity : false;
  
unset($opp_list);

       
        //Requests /opportunity/id:opportunity/customfield
        try {
            $customField = $capsule->opportunity->CustomField->getAll(2038821);
        } catch (Services_Capsule_Exception $e) {
            //print_r($e);
        }

        foreach ($customField as $key => $value) {
            $customField = $value;
            $customField = $customField->customField;
        }

        //Reindex oppArray to have the same ID as its object data.
        foreach ($opportunity_list as $key => $value) {
            unset($opportunity_list[$key]);
            $opportunity_list[$value->id] = $value;
        }
        foreach ($opportunity_list as $key => &$value)
        {
            try {
                $party = $capsule->party->get($value->partyId);
            } catch (Services_Capsule_Exception $e) {
                //print_r($e);
            }

            //Stores the $value in a new array
            //Parses from stdObject array type
            $name = (array)$value;

            //Creates a new value partyId
            //Queried as firstName, lastName, and organisation name
            $name['partyId'] = "";

            if (isset($party->person)) {
                $name['partyId'] .= $party->person->firstName . ' ';
                $name['partyId'] .= (isset($party->person->lastName)) ? $party->person->lastName : '';
            }

            if (isset($party->organisation)){
                $name['partyId'] .= $party->organisation->name;
            }

            $name['createdOn'] = date('Y/m/d', strtotime($name['createdOn']));

            // Hardcoded ID for a specific custom field
            if ($value->id == 2038821) {
                $name['comments'] = $customField->id .= " ";
                $name['comments'] .= $customField->label .= " ";
                $name['comments'] .= $customField->text .= " ";
            }
            

            // Change actual array data ($value should be a reference, not a copy)
            $value = $name;
        }
        ?>

<html>

<head>
   
    <link rel="stylesheet" href="docs/css/jq.css" type="text/css" media="print, projection, screen" />
    <link rel="stylesheet" href="themes/blue/style.css" type="text/css" media="print, projection, screen" />
    <script type="text/javascript" src="jquery-latest.js"></script> 
    <script type="text/javascript" src="jquery.tablesorter.js"></script> 
    <script type="text/javascript" src="addons/pager/jquery.tablesorter.pager.js"></script> 
    <script type="text/javascript" src="docs/js/docs.js"></script>
  
    <title>Opportunity</title>
</head>
<body>
<h1 style="text-align: center;">Opportunities Table</h1>
<div>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="2">
    <thead>
    <tr>
        <th>Party Id</th>
        <th>Opportunity&nbsp</th>
        <th>Description</th>
        <th>Milestone</th>
        <th>Comments&nbsp</th>
        <th>Value</th>
        <th>Date Created</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($opportunity_list as $k => $v) {
        if (is_object($v)) {
            $v = (array) $v;
        }

        if (!isset($v['description'])) {
            $v['description'] = ' ';
        }

        if (!isset($v['comments'])) {
            $v['comments'] = ' ';
        }

        $table_html = "
                <td>{$v['partyId']}</td>
                <td>{$v['name']}</td>
                <td>{$v['description']}</td>
                <td>{$v['milestone']}</td>
                <td>{$v['comments']}</td>
                <td>{$v['value']}</td>
                <td>{$v['createdOn']}</td>
             ";

        echo "<tr>{$table_html}</tr>";
    }
    
    ?>
    
  
    </tbody>  
</table>  
</div>
   <script> 
        $(document).ready(function() 
    { 
        $("#myTable").tablesorter({ 
        // sort on the first column and third column, order asc 
        sortList: [[6,1]] 
        })
    .tablesorterPager({container: $("#pager")}); 
    } 
); 
    
    </script>
    <div id="pager" class="pager">
	<form>
		<img src="addons/pager/first.png" class="first"/>
		<img src="addons/pager/prev.png" class="prev"/>
		<input type="text" class="pagedisplay"/>
		<img src="addons/pager/next.png" class="next"/>
		<img src="addons/pager/last.png" class="last"/>
		<select class="pagesize">
			<option selected="selected"  value="10">10</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option  value="40">40</option>
		</select>
	</form>
</div>
</body>

</html>
